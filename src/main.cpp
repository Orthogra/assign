#include <cassert>
#include <cstdio>
#include <iostream>
#include <memory>
#include <cstring>
#include <map>
#include <set>
#include <stack>
#include <vector>
#include <algorithm>
#include <unistd.h>
#include "assert.h"  
#include "koopa.h"
#include "AST.h"

using namespace std;

void parse_string(const char* str);
void global_alloc(koopa_raw_value_t value);
int calc_alloc_size(koopa_raw_type_t value);

// 声明 lexer 的输入, 以及 parser 函数
// 为什么不引用 sysy.tab.hpp 呢? 因为首先里面没有 yyin 的定义
// 其次, 因为这个文件不是我们自己写的, 而是被 Bison 生成出来的
// 你的代码编辑器/IDE 很可能找不到这个文件, 然后会给你报错 (虽然编译不会出错)
// 看起来会很烦人, 于是干脆采用这种看起来 dirty 但实际很有效的手段
extern FILE *yyin;
extern int yyparse(unique_ptr<BaseAST> &ast);
extern void parse_string(const char* str);

int main(int argc, const char *argv[]) {
  // 解析命令行参数. 测试脚本/评测平台要求你的编译器能接收如下参数:
  // compiler 模式 输入文件 -o 输出文件
  assert(argc == 5);
  assert(argc == 5);
  auto mode = argv[1];
  auto input = argv[2];
  auto output = argv[4];

  // 打开输入文件, 并且指定 lexer 在解析的时候读取这个文件
  yyin = fopen(input, "r");
  assert(yyin);
  // 调用 parser 函数, parser 函数会进一步调用 lexer 解析输入文件的
  // parse input file
  unique_ptr<BaseAST> ast;
  auto ret = yyparse(ast);
  assert(!ret);

  int old = dup(1);

  if (strcmp(mode, "-koopa") == 0)
  {
    // Dump Koopa IR
    freopen(output, "w", stdout);
    ast->Dump();
    dup2(old, 1);
  }
  else
  {
    // Dump Koopa IR
    FILE *fp = freopen((string(output) + ".koopa").c_str(), "w", stdout);
    ast->Dump();
    fflush(fp);
    dup2(old, 1);
    // Koopa IR -> RISC-V
    FILE* koopaio = fopen((string(output) + ".koopa").c_str(), "r");
    char *str=(char *)malloc(1 << 30);
    memset(str, 0, 1 << 30);
    int len = fread(str, 1, 1 << 30, koopaio);
    str[len] = 0;
    freopen(output, "w", stdout);
    parse_string(str);
    dup2(old, 1);
  }

  return 0;
}

void global_alloc(koopa_raw_value_t value)
{
  for (int i = 0; i < value->kind.data.aggregate.elems.len; ++i)
  {
    koopa_raw_value_t t = (koopa_raw_value_t)value->kind.data.aggregate.elems.buffer[i];
    switch (t->kind.tag)
    {
      case KOOPA_RVT_INTEGER:
      cout << "  .word " << t->kind.data.integer.value << endl;
        break;
      default:
        global_alloc(t);
        break;
    }
  }
}

int calc_alloc_size(koopa_raw_type_t value)
{
  if (value->tag == 0)
    return 4;
  else
    return value->data.array.len * calc_alloc_size(value->data.array.base);
}

void parse_string(const char* str)
{
  map<koopa_raw_value_t, int> map;
  koopa_program_t program;
  koopa_error_code_t ret = koopa_parse_from_string(str, &program);
  assert(ret == KOOPA_EC_SUCCESS);
  koopa_raw_program_builder_t builder = koopa_new_raw_program_builder();
  koopa_raw_program_t raw = koopa_build_raw_program(builder, program);
  koopa_delete_program(program);
  int local_label = 1;


  for (size_t i = 0; i < raw.values.len; ++i)
  {
    koopa_raw_value_t value = (koopa_raw_value_t) raw.values.buffer[i];
    cout << "  .data" << endl;
    cout << "  .globl " << value->name+1 << endl;
    cout<<value->name+1<<":"<<endl;
    
    if (value->kind.tag == KOOPA_RVT_GLOBAL_ALLOC)
    {
      if (value->kind.data.global_alloc.init->kind.tag == KOOPA_RVT_AGGREGATE)
      {
        global_alloc(value->kind.data.global_alloc.init);
      }
      if (value->kind.data.global_alloc.init->kind.tag == KOOPA_RVT_ZERO_INIT)
      {        
        cout << "  .zero " << calc_alloc_size(value->kind.data.global_alloc.init->ty)<< endl;
      }
      if (value->kind.data.global_alloc.init->kind.tag == KOOPA_RVT_INTEGER)
      {
        cout<<"  .word "<<value->kind.data.global_alloc.init->kind.data.integer.value<<endl;
      }
    }
    cout<<endl;
  }

  for (size_t i = 0; i < raw.funcs.len; ++i) {
    assert(raw.funcs.kind == KOOPA_RSIK_FUNCTION);
    koopa_raw_function_t func = (koopa_raw_function_t) raw.funcs.buffer[i];

    if (func->bbs.len == 0)
      continue;
    cout << endl << "  .text"<< endl;
    cout << "  .globl " << func->name+1 << endl; 
    cout << func->name+1 << ":" << endl;

    int size = 0;
    for (size_t j = 0; j < func->bbs.len; ++j) {
      koopa_raw_basic_block_t bb = (koopa_raw_basic_block_t)func->bbs.buffer[j];
      for (size_t k = 0; k < bb->insts.len; ++k){
        koopa_raw_value_t value = (koopa_raw_value_t)bb->insts.buffer[k];

        if (value->kind.tag == KOOPA_RVT_ALLOC){
          if (value->ty->tag == KOOPA_RTT_POINTER)
          {
            if (value->ty->data.pointer.base->tag == KOOPA_RTT_INT32)
            {
              size += 4;
            }
            else if (value->ty->data.pointer.base->tag == KOOPA_RTT_ARRAY)
            {
              int sz = calc_alloc_size(value->ty->data.pointer.base);
              size += sz;
            }
            else if (value->ty->data.pointer.base->tag == KOOPA_RTT_POINTER)
            {
              size += 4;
            }
          }    
        }
        else if (value->kind.tag == KOOPA_RVT_LOAD){
            size += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_STORE){
        }
        else if (value->kind.tag == KOOPA_RVT_GET_PTR) {
          size += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_GET_ELEM_PTR) {
          size += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_BINARY){
          size += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_BRANCH){
        }
        else if (value->kind.tag == KOOPA_RVT_JUMP){
        }
        else if (value->kind.tag == KOOPA_RVT_CALL){
          size += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_RETURN){
        }
      }
    }

    size = (size + 31) >> 4 << 4;
    if (size >= -2048 && size <=2047)
    {
      cout << "  addi  sp, sp, -" << size <<endl;
      cout << "  sw    ra, " << size - 4 << "(sp)" << endl;
      cout << "  sw    s0, " << size - 8 << "(sp)" << endl;
    }
    else
    {
      cout << "  li    t0, " << size << endl;
      cout << "  sub   sp, sp, t0" << endl;
      cout << "  add   t1, sp, t0" << endl;
      cout << "  sw    ra, " << -4 << "(t1)" << endl;
      cout << "  sw    s0, " << -8 << "(t1)" << endl;
    }
    
    int allc = 0;

    for (size_t j = 0; j < func->bbs.len; ++j) {
      assert(func->bbs.kind == KOOPA_RSIK_BASIC_BLOCK);
      koopa_raw_basic_block_t bb = (koopa_raw_basic_block_t)func->bbs.buffer[j];
      cout << bb->name+1 << ":" << endl;
      for (size_t k = 0; k < bb->insts.len; ++k){
        int current = 0;
        koopa_raw_value_t value = (koopa_raw_value_t)bb->insts.buffer[k];

        if (value->kind.tag == KOOPA_RVT_ALLOC){
          if (value->ty->tag == KOOPA_RTT_POINTER)
          {
            if (value->ty->data.pointer.base->tag == KOOPA_RTT_INT32)
            {
              map[value] = allc;
              allc = allc + 4;
            }
            else if (value->ty->data.pointer.base->tag == KOOPA_RTT_ARRAY)
            {
              int sz = calc_alloc_size(value->ty->data.pointer.base);
              map[value] = allc;
              allc += sz;
            }
            else if (value->ty->data.pointer.base->tag == KOOPA_RTT_POINTER)
            {
              map[value] = allc;
              allc = allc + 4;
            }
          }
        }
        else if (value->kind.tag == KOOPA_RVT_LOAD){
          if (value->kind.data.load.src->kind.tag == KOOPA_RVT_GLOBAL_ALLOC){
            cout << "  la    t0, " << value->kind.data.load.src->name+1 << endl;
            cout << "  lw    t0, 0(t0)" << endl;
            if (allc >= -2048 && allc <=2047)
            {
              cout << "  sw    t0, " << allc << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t1, " << allc << endl;
              cout << "  add   t1, t1, sp" << endl;
              cout << "  sw    t0, 0(t1)" << endl;
            }            
            map[value] = allc;
            allc = allc + 4;
          }
          else if (value->kind.data.load.src->kind.tag == KOOPA_RVT_ALLOC)
          {
            if (map[value->kind.data.load.src] >= -2048 && map[value->kind.data.load.src] <=2047)
            {
              cout << "  lw    t0, " << map[value->kind.data.load.src] << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t1, " << map[value->kind.data.load.src] << endl;
              cout << "  add   t1, t1, sp" << endl;
              cout << "  lw    t0, 0(t1)" << endl;
            }
            
            if (allc >= -2048 && allc <=2047)
            {
              cout << "  sw    t0, " << allc << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t1, " << allc << endl;
              cout << "  add   t1, t1, sp" << endl;
              cout << "  sw    t0, 0(t1)" << endl;
            }
            map[value] = allc;
            allc = allc + 4;
          }
          else{
            if (map[value->kind.data.load.src] >= -2048 && map[value->kind.data.load.src] <=2047)
            {
              cout << "  lw    t0, " << map[value->kind.data.load.src] << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t1, " << map[value->kind.data.load.src] << endl;
              cout << "  add   t1, t1, sp" << endl;
              cout << "  lw    t0, 0(t1)" << endl;
            }

            cout << "  lw    t0, 0(t0)" << endl;
            
            if (allc >= -2048 && allc <=2047)
            {
              cout << "  sw    t0, " << allc << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t1, " << allc << endl;
              cout << "  add   t1, t1, sp" << endl;
              cout << "  sw    t0, 0(t1)" << endl;
            }

            map[value] = allc;
            allc = allc + 4;
          }
        }
        else if (value->kind.tag == KOOPA_RVT_STORE){
          if(value->kind.data.store.value->kind.tag == KOOPA_RVT_INTEGER){
            if (value->kind.data.store.dest->kind.tag == KOOPA_RVT_GLOBAL_ALLOC)
            {
              cout << "  la    t0, " << value->kind.data.store.dest->name+1 << endl;
              cout << "  li    t1, " << value->kind.data.store.value->kind.data.integer.value << endl;
              cout << "  sw    t1, 0(t0)" << endl;
            }
            else if (value->kind.data.store.dest->kind.tag == KOOPA_RVT_ALLOC)
            {
              cout << "  li    t0, " << value->kind.data.store.value->kind.data.integer.value << endl;

              if (map[value->kind.data.store.dest] >= -2048 && map[value->kind.data.store.dest] <= 2047)
              {
                cout << "  sw    t0, " << map[value->kind.data.store.dest] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t1, " << map[value->kind.data.store.dest]<<endl;
                cout << "  add   t1, t1, sp" << endl;
                cout << "  sw    t0, 0(t1)" << endl;
              }              
            }
            else
            {
              cout << "  li    t0, " << value->kind.data.store.value->kind.data.integer.value << endl;

              if (map[value->kind.data.store.dest] >= -2048 && map[value->kind.data.store.dest] <= 2047)
              {
                cout << "  lw    t1, " << map[value->kind.data.store.dest] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t1, " << map[value->kind.data.store.dest]<<endl;
                cout << "  add   t1, t1, sp" << endl;
                cout << "  lw    t1, 0(t1)" << endl;
              }  
              cout << "  sw    t0, 0(t1)" << endl;              
            }
            
          }
          else if (value->kind.data.store.value->kind.tag == KOOPA_RVT_FUNC_ARG_REF) 
          {
            int id = value->kind.data.store.value->kind.data.block_arg_ref.index;
            if (id < 8)
            {
              if (map[value->kind.data.store.dest] >=-2048 && map[value->kind.data.store.dest] <=2047)
              {
                cout << "  sw    a" << id << ", " << map[value->kind.data.store.dest] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t1, " << map[value->kind.data.store.dest] <<endl;
                cout << "  add   t1, t1, sp" << endl;
                cout << "  sw    a" << id << ", 0(t1)" << endl;
              }
              
            }
            else
            {
              if (size + 4 * (id - 8) >= -2048 && size + 4 * (id - 8) <= 2047)
              {
                cout << "  lw    t0, " << size + 4 * (id - 8) << "(sp)"<< endl;
              }
              else
              {
                cout << "  li    t0, " << size + 4 * (id - 8);
                cout << "  add   t0, t0, sp" << endl;
                cout << "  lw    t0, 0(t0)" << endl;
              }

              if (map[value->kind.data.store.dest] >= -2048 && map[value->kind.data.store.dest] <= 2047)
              {
                cout << "  sw    t0, " << map[value->kind.data.store.dest] << "(sp)" << endl;
              }
              else
              {
                cout << "  li   t1, " << map[value->kind.data.store.dest];
                cout << "  add  t1, t1, sp" << endl;
                cout << "  sw   t0, 0(t1)" <<endl;
              }              
            }
          }
          else
          {
            if (value->kind.data.store.dest->kind.tag == KOOPA_RVT_GLOBAL_ALLOC)
            {
              cout << "  la    t0, " << value->kind.data.store.dest->name+1 << endl;
              if (map[value->kind.data.store.value] >= -2048 && map[value->kind.data.store.value] <= 2047)
              {
                cout << "  lw    t1, " << map[value->kind.data.store.value] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t1, " << map[value->kind.data.store.value] << endl;
                cout << "  add   t1, t1, sp" << endl;
                cout << "  lw    t1, 0(t1)" << endl;
              }
              cout << "  sw    t1, 0(t0)" << endl;
            }
            else if (value->kind.data.store.dest->kind.tag == KOOPA_RVT_ALLOC)
            {
              if (map[value->kind.data.store.value] >= -2048 && map[value->kind.data.store.value] <= 2047)
              {
                cout << "  lw    t1, " << map[value->kind.data.store.value] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t1, " << map[value->kind.data.store.value] << endl;
                cout << "  add   t1, t1, sp" << endl;
                cout << "  lw    t1, 0(t1)" << endl;
              }
              if (map[value->kind.data.store.dest] >= -2048 && map[value->kind.data.store.dest] <=2047)
              {
                cout << "  sw    t1, " << map[value->kind.data.store.dest] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t0, " << map[value->kind.data.store.dest] << endl;
                cout << "  add   t0, t0, sp" << endl;
                cout << "  sw    t1, 0(t0)" << endl;
              }
            }
            else
            {
              if (map[value->kind.data.store.value] >= -2048 && map[value->kind.data.store.value] <= 2047)
              {
                cout << "  lw    t0, " << map[value->kind.data.store.value] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t0, " << map[value->kind.data.store.value] << endl;
                cout << "  add   t0, t0, sp" << endl;
                cout << "  lw    t0, 0(t0)" << endl;
              }
              if (map[value->kind.data.store.dest] >= -2048 && map[value->kind.data.store.dest] <=2047)
              {
                cout << "  lw    t1, " << map[value->kind.data.store.dest] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t1, " << map[value->kind.data.store.dest] <<endl;
                cout << "  add   t1, t1, sp" << endl;
                cout << "  lw    t1, 0(t1)" << endl;
              }
              cout << "  sw    t0, 0(t1)" << endl;              
            }
          }
        }
        else if (value->kind.tag == KOOPA_RVT_GET_PTR) {
          koopa_raw_value_t src = value->kind.data.get_ptr.src;
          koopa_raw_value_t index = value->kind.data.get_ptr.index;
          int sz;
          if (src->kind.tag == KOOPA_RVT_ALLOC)
          {
            if (map[src] <= 2047 && map[src] >= -2048)
              cout << "  addi  t0, sp, " << map[src] << endl;
            else
            {
              cout << "  li    t1, " << map[src] << endl;
              cout << "  add   t0, sp, t1" << endl;
            }
            sz = calc_alloc_size(src->ty->data.pointer.base->data.array.base);
          }
          else if(src->kind.tag == KOOPA_RVT_GLOBAL_ALLOC)
          {
            cout << "  la    t0, " << src->name+1 << endl;
            sz = calc_alloc_size(src->ty->data.pointer.base->data.array.base);
          }
          else if (src->kind.tag == KOOPA_RVT_LOAD)
          {
            if (map[src] <= 2047 && map[src] >= -2048)
              cout << "  addi  t0, sp, " << map[src] << endl;
            else
            {
              cout << "  li    t1, " << map[src] << endl;
              cout << "  add   t0, sp, t1" << endl;
            }
              cout << "  lw    t0, 0(t0)" << endl;
            sz = calc_alloc_size(src->kind.data.load.src->ty->data.pointer.base->data.pointer.base);
          }
          else
          {
            if (map[src] <= 2047 && map[src] >= -2048)
              cout << "  addi  t0, sp, " << map[src] << endl;
            else
            {
              cout << "  li    t1, " << map[src] << endl;
              cout << "  add   t0, sp, t1" << endl;
            }
              cout << "  lw    t0, 0(t0)" << endl;
            sz = calc_alloc_size(src->ty->data.pointer.base->data.array.base);

          }
          if (index->kind.tag == KOOPA_RVT_INTEGER)
          {
            int offset = index->kind.data.integer.value;
            cout << "  li    t1, " << offset << endl;
          }
          else
          {
            if (map[index] >= -2048 && map[index] <= 2047)
            {
              cout << "  lw    t1, " << map[index] << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t1, " << map[index] << endl;
              cout << "  add   t1, t1, sp" << endl;
              cout << "  lw    t1, 0(t1)" << endl;
            }
          }
          cout << "  li    t2, " << sz << endl;
          cout << "  mul   t1, t1, t2" << endl;
          cout << "  add   t0, t0, t1" << endl;
          
          if (allc >= -2048 && allc <=2047)
          {
            cout << "  sw    t0, " << allc << "(sp)" << endl;
          }
          else
          {
            cout << "  li    t1, " << allc <<endl;
            cout << "  add   t1,t1,sp"<<endl;
            cout << "  sw    t0, 0(t1)" << endl;
          }
          
          map[value] = allc;
          allc += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_GET_ELEM_PTR) {
          koopa_raw_value_t src = value->kind.data.get_elem_ptr.src;
          koopa_raw_value_t index = value->kind.data.get_elem_ptr.index;
          int sz = calc_alloc_size(src->ty->data.pointer.base->data.array.base);
          if (src->kind.tag == KOOPA_RVT_ALLOC)
          {
            if (map[src] <= 2047 && map[src] >= -2048)
              cout<< "  addi   t0, sp, " << map[src] << endl;
            else
            {
              cout << "  li    t1, " << map[src] << endl;
              cout << "  add   t0, sp, t1" << endl;
            }
          }
          else if(src->kind.tag == KOOPA_RVT_GLOBAL_ALLOC)
          {
            cout << "  la    t0, " << src->name+1 << endl;
          }
          else
          {
            if (map[src] <= 2047 && map[src] >= -2048)
              cout << "  addi  t0, sp, " << map[src] << endl;
            else
            {
              cout << "  li    t1, " << map[src] << endl;
              cout << "  add   t0, sp, t1" << endl;
            }
              cout << "  lw  t0, 0(t0)" << endl;
            
          }
          if (index->kind.tag == KOOPA_RVT_INTEGER)
          {
            int offset = index->kind.data.integer.value;
            cout << "  li   t1, " << offset << endl;
          }
          else
          {
            if (map[index] >= -2048 && map[index] <= 2047)
            {
              cout << "  lw    t1, " << map[index] << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t1, " << map[index] << endl;
              cout << "  add   t1, t1, sp" << endl;
              cout << "  lw    t1, 0(t1)" << endl;
            }
          }
          
          cout << "  li    t2, " << sz << endl;
          cout << "  mul   t1, t1, t2" << endl;
          cout << "  add   t0, t0, t1" << endl;
          
          if (allc >= -2048 && allc <= 2047)
          {
            cout << "  sw    t0, " << allc << "(sp)" << endl;
          }
          else
          {
            cout << "  li    t1, " << allc <<endl;
            cout << "  add   t1,t1,sp"<<endl;
            cout << "  sw    t0, 0(t1)" << endl;
          }

          map[value] = allc;
          allc += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_BINARY){
          string rs1,rs2,rd;
          if(value->kind.data.binary.lhs->kind.tag == KOOPA_RVT_INTEGER){
            int lvalue = value->kind.data.binary.lhs->kind.data.integer.value;
            if (lvalue == 0){
              rs1 = "x0";
            }
            else
            {
              cout << "  li    " << "t" << current << ", " << lvalue << endl;
              rs1 = string("t") + to_string(current);
              current = current + 1;
            }
          }
          else {
            if (map[value->kind.data.binary.lhs] >= -2048 && map[value->kind.data.binary.lhs] <= 2047)
            {
              cout << "  lw    t" << current << ", " << map[value->kind.data.binary.lhs] <<"(sp)" << endl;
            }
            else
            {
              cout << "  li    t" << current << ", " << map[value->kind.data.binary.lhs] << endl;
              cout << "  add   t" << current << ", t" << current << ", sp" << endl;
              cout << "  lw    t" << current << ", 0(t" << current << ")" << endl; 
            }
            
            rs1 = string("t") + to_string(current);
            current = current + 1;
          }
          // cout<< "op = "<< value->kind.data.binary.op <<' '<<endl;
          if(value->kind.data.binary.rhs->kind.tag == KOOPA_RVT_INTEGER){
            int rvalue = value->kind.data.binary.rhs->kind.data.integer.value;
            if (rvalue == 0){
              rs2 = "x0";
            }
            else{
              cout << "  li    " << "t" << current << ", " << rvalue << endl;
              rs2 = string("t") + to_string(current);
              current = current + 1;
            }
          }
          else {
            if (map[value->kind.data.binary.rhs] >= -2048 && map[value->kind.data.binary.rhs] <= 2047)
            {
              cout << "  lw    t" << current << ", " << map[value->kind.data.binary.rhs] <<"(sp)" << endl;
            }
            else
            {
              cout << "  li    t" << current << ", " << map[value->kind.data.binary.rhs] << endl;
              cout << "  add   t" << current << ", t" << current << ", sp" << endl;
              cout << "  lw    t" << current << ", 0(t" << current << ")" << endl; 
            }
            rs2 = string("t") + to_string(current);
            current = current + 1;
          }    
        
          rd = string("t") + to_string(current);
          current = current + 1;
          // map[value] = rd;
          if (value->kind.data.binary.op == 0){
            // ne          
            cout<<"  xor   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
            cout<<"  seqz  "<< rd<<", "<<rd<<endl;
            cout << "  li    " << rs1 << ", " << 1 << endl;
            cout<<"  sub   "<< rd<<", "<<rs1<<", "<< rd<<endl;
          }
          else if (value->kind.data.binary.op == 1){
            // eq          
            cout<<"  xor   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
            cout<<"  seqz  "<< rd<<", "<<rd<<endl;
          }
          else if (value->kind.data.binary.op == 2){
            cout<<"  sgt   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 3){
            cout<<"  slt   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 4){
            cout<<"  slt   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
            cout<<"  seqz  "<< rd<<", "<<rd<<endl;
          }
          else if (value->kind.data.binary.op == 5){
            cout<<"  sgt   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
            cout<<"  seqz  "<< rd<<", "<<rd<<endl;
          }
          else if (value->kind.data.binary.op == 6){
            cout<<"  add   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 7){
            cout<<"  sub   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 8){
            cout<<"  mul   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 9){
            cout<<"  div   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 10){
            cout<<"  rem   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 11){
            cout<<"  and   "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }
          else if (value->kind.data.binary.op == 12){
            cout<<"  or    "<< rd<<", "<<rs1<<", "<<rs2<<endl;
          }

          if (allc >= -2048 && allc <= 2047)
          {
            cout << "  sw    " << rd <<", " << allc << "(sp)" << endl;
          }
          else
          {
            cout << "  li    t" << current << ", " << allc << endl;
            cout << "  add   t" << current << ", t" << current << ", sp" << endl;
            cout << "  sw    " << rd << ", 0(t" << current << ")" << endl;
          }
          map[value] = allc;
          allc += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_BRANCH){
          if (map[value->kind.data.branch.cond] >= -2048 && map[value->kind.data.branch.cond] <=2047)
          {
            cout << "  lw    t0, " << map[value->kind.data.branch.cond] << "(sp)" << endl;
          }
          else
          {
            cout << "  li    t0, " << map[value->kind.data.branch.cond] << endl;
            cout << "  add   t0, t0, sp" << endl;
            cout << "  lw    t0, 0(t0)" << endl;
          }
          
          cout << "  beq   t0, x0, " << local_label << "f" << endl;
          cout << "  jal   x0, " << value->kind.data.branch.true_bb->name+1 << endl;
          cout << local_label++ << ":  jal   x0, " << value->kind.data.branch.false_bb->name+1 << endl;
        }
        else if (value->kind.tag == KOOPA_RVT_JUMP){
          cout << local_label << ":  auipc t1, %pcrel_hi(" << value->kind.data.jump.target->name+1 << ")" << endl;
          cout << "  jalr  x0, %pcrel_lo(" << local_label++ << "b) (t1)" << endl;

        }
        else if (value->kind.tag == KOOPA_RVT_CALL){
          int sbrk = 0;
          int p1 = value->kind.data.call.args.len > 8 ? 8:value->kind.data.call.args.len;
          for (int para = 0; para < p1; ++para)
          {
            if (map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] >= -2048 && map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] <=2047)
            {
              cout<< "  lw   a" << para << ", " << map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] << "(sp)" << endl;
            }
            else
            {
              cout << "  li    t0," << map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] << endl;
              cout << "  add   t0, t0, sp" << endl;
              cout << "  lw    a" << para << ", 0(t0)" << endl; 
            }
            
          }
          if (value->kind.data.call.args.len > 8)
          {
            sbrk = 4 * (value->kind.data.call.args.len - 8);
            cout<< "  addi  sp, sp, -" << sbrk << endl; 
          }
          if (sbrk)
          {
            for (int para = 8; para < value->kind.data.call.args.len; ++para)
            {
              if (map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] >= -2048 && map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] <=2047)
              {
                cout<< "  lw    t0, " << map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] + sbrk << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t0," << map[(koopa_raw_value_t)value->kind.data.call.args.buffer[para]] << endl;
                cout << "  add   t0, t0, sp" << endl;
                cout << "  lw    t0, " << sbrk << "(t0)" << endl; 
              }

              cout<< "  sw    t0, " << 4 * (para-8) << "(sp)" << endl; 
            }
          }
          cout << "  call  " << value->kind.data.call.callee->name+1 << endl;
          if (sbrk){
            cout<< "  addi  sp, sp, " << sbrk << endl;
          }
          if (allc >=-2048 && allc <= 2047)
          {
            cout<< "  sw    a0, " << allc << "(sp)" << endl;
          }
          else
          {
            cout << "  li    t0, " << allc <<endl;
            cout << "  add  t0, t0, sp" << endl;
            cout << "  sw    a0, 0(t0)" << endl;
          }          
          map[value] = allc;
          allc += 4;
        }
        else if (value->kind.tag == KOOPA_RVT_RETURN){
          koopa_raw_value_t ret_value = value->kind.data.ret.value;
          if (ret_value != NULL){
            if (ret_value->kind.tag == KOOPA_RVT_INTEGER){
              int32_t int_val = ret_value->kind.data.integer.value;
              cout << "   li    " << "a0 , " << int_val << endl;
            }
            else {
              if (map[ret_value] >= -2048 && map[ret_value] <=2047)
              {
                cout << "  lw    a0, "<< map[ret_value] << "(sp)" << endl;
              }
              else
              {
                cout << "  li    t0, " << map[ret_value] << endl;
                cout << "  add   t0, t0, sp" << endl;
                cout << "  lw    a0, 0(t0)" << endl;
              }
            }
          }
          
          if (size >= -2048 && size <= 2047)
          {
            cout << "  lw    ra, " << size - 4 << "(sp)" << endl;
            cout << "  lw    s0, " << size - 8 << "(sp)" << endl;
            cout << "  addi  sp, sp, " << size <<endl;
          }
          else
          {
            cout << "  li    t0, " << size << endl;
            cout << "  add   t1, t0, sp" << endl;
            cout << "  lw    ra, " << -4 << "(t1)" << endl;
            cout << "  lw    s0, " << -8 << "(t1)" << endl;
            cout << "  add  sp, sp, t0" << endl;
          }          
          cout << "  ret" << endl;
        }
      }
    }
  }
  koopa_delete_raw_program_builder(builder);
}